package main

import (
    "fmt"
    "io/ioutil"
)

func main() {
    f1()
}

func cherr(err error) {
    if err != nil {
	panic(err)
    }
}

func f1() {
    b, err := ioutil.ReadFile("ch1")
    cherr(err)
    str := string(b)
    fmt.Printf("%s\n", str[0:6])
}
